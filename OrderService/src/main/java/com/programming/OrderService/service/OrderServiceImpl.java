package com.programming.OrderService.service;

import com.programming.OrderService.entity.Order;
import com.programming.OrderService.exception.CustomException;
import com.programming.OrderService.external.client.ProductService;
import com.programming.OrderService.model.OrderRequest;
import com.programming.OrderService.model.OrderResponse;
import com.programming.OrderService.repository.OrderRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;

@Service
@Log4j2
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ProductService productService;

    @Override
    public long placeOrder(OrderRequest orderRequest) {
        /** Ideally we can create the order entity and save the data with status order created.
         * After that, we can call the product service to block our products to reduce the quantity.
         * Once we do above, we need to call the payment service to complete the payment as well. And if the
         * payment is successful, we can mark the order as complete else we can mark this as cancel.
         */
        log.info("Placing Order Request: {}", orderRequest);
        productService.reduceQuantity(orderRequest.getProductId(), orderRequest.getQuantity());
        log.info("Creating Order with status CREATED");

        Order order = Order.builder()
                .amount(orderRequest.getTotalAmount())
                .orderStatus("CREATED")
                .productId(orderRequest.getProductId())
                .orderDate(Instant.now())
                .quantity(orderRequest.getQuantity())
                .build();

        order = orderRepository.save(order);
        log.info("Order places successfully with Order Id: {}", order.getId());
        return order.getId();
    }

    @Override
    public OrderResponse getOrderDetails(long orderId) {
        log.info("Get order details for Order Id : {}", orderId);
        Order order = orderRepository.findById(orderId).orElseThrow(() -> new CustomException(
                "Order not found for the order Id:" + orderId, "NOT_FOUND", 404
        ));
        OrderResponse orderResponse = OrderResponse.builder()
                .orderId(order.getId())
                .orderStatus(order.getOrderStatus())
                .amount(order.getAmount())
                .orderDate(order.getOrderDate())
                .build();
        return orderResponse;
    }
}
