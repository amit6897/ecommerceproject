package com.programming.OrderService.service;

import com.programming.OrderService.model.OrderRequest;
import com.programming.OrderService.model.OrderResponse;

public interface OrderService {
    long placeOrder(OrderRequest orderRequest);

    OrderResponse getOrderDetails(long orderId);
}
