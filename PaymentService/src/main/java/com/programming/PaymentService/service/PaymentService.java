package com.programming.PaymentService.service;

import com.programming.PaymentService.model.PaymentRequest;

public interface PaymentService {
    long doPayment(PaymentRequest request);
}
