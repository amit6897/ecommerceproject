package com.programming.ProductService.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
/*
@Builder -> It will give us the builder pattern implementation of this class.
With the help of builder pattern, we can add all the properties in the product whenever we are using it. So i don't have to worry about calling
the entire constructor itself if i want, i just want to add a couple of fields. I can use that using builder pattern as well.
*/
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    // @GeneratedValue -> We want this product to be incremented one by one for each and every product been inserted. It will be automatically generated on the sequence itself.
    private long productId;

    @Column(name = "PRODUCT_NAME")
    private String productName;

    @Column(name = "PRICE")
    private long price;

    @Column(name = "Quantity")
    private long quantity;
}

