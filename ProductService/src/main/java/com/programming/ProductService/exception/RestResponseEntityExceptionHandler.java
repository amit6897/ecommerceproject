package com.programming.ProductService.exception;

import com.programming.ProductService.model.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ProductServiceCustomException.class)
    public ResponseEntity<ErrorResponse> handleProductServiceException(ProductServiceCustomException exception) {
        return new ResponseEntity<>(new ErrorResponse().builder()
                .errorMessage(exception.getMessage())
                .errorCode(exception.getErrorCode())
                .build(), HttpStatus.NOT_FOUND);
    }
}

/*
In ServiceImpl, we have added ProductServiceCustomException by replacing RuntimeException.
Alongside this, we need to handle the exception as well. For that, we have to add the controller, advise
along the controller to handle all the exceptions, so a proper response is sent back to the client.
So we have created this configuration class.

@ControllerAdvice -> Whenever there is exception on my controller, this will handle it.
*/