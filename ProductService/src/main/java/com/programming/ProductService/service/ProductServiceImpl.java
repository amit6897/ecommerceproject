package com.programming.ProductService.service;

import com.programming.ProductService.entity.Product;
import com.programming.ProductService.exception.ProductServiceCustomException;
import com.programming.ProductService.model.ProductRequest;
import com.programming.ProductService.model.ProductResponse;
import com.programming.ProductService.repository.ProductRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static org.springframework.beans.BeanUtils.*;

@Service
@Log4j2
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public long addProduct(ProductRequest productRequest) {
        log.info("Adding Product....");
        Product product = Product.builder()
                .productName(productRequest.getName())
                .quantity(productRequest.getQuantity())
                .price(productRequest.getPrice())
                .build();
        productRepository.save(product);
        log.info("Product Created....");
        return product.getProductId();
    }

    @Override
    public ProductResponse getProductById(long productId) {
        log.info("Get the product for productId: {}", productId);
        Product product = productRepository.findById(productId)     // findById will return the optional
                .orElseThrow(()->new ProductServiceCustomException("Product with given id not found", "PRODUCT_NOT_FOUND"));

        /** Suppose we got the product and there is no exception. So, we need to convert this product into
         * my product response object here.
         * For that, I can use the builder pattern to create the object here, or I can use the bean utils
         * as well to create our object.
         */
        ProductResponse productResponse = new ProductResponse();
        //BeanUtils.copyProperties(product, productResponse);
        copyProperties(product, productResponse);   // I have added on demand static import for BeanUtils

        return productResponse;
    }

    @Override
    public void reduceQuantity(long productId, long quantity) {
        log.info("Reduce Quantity {} for Id: {}", quantity, productId);
        Product product = productRepository.findById(productId)
                .orElseThrow(()->new ProductServiceCustomException(
                        "Product with given id not found", "PRODUCT_NOT_FOUND"));
        if (product.getQuantity() < quantity) {
            throw new ProductServiceCustomException(
                    "Product does not have specific quantity", "INSUFFICIENT_QUANTITY");
        }
        product.setQuantity(product.getQuantity() - quantity);
        productRepository.save(product);
        log.info("Product Quantity updated successfully");
    }
}
