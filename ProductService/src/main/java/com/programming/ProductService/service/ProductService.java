package com.programming.ProductService.service;

import com.programming.ProductService.model.ProductRequest;
import com.programming.ProductService.model.ProductResponse;

public interface ProductService {
    long addProduct(ProductRequest productRequest);

    ProductResponse getProductById(long productId);

    void reduceQuantity(long productId, long quantity);
}
